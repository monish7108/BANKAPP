import java.io.IOException;
import java.sql.*;


public abstract class Commit {
	
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/";
	static final String USER = "root";
	static final String PASS = "tarams";
	
	Connection conn = null;
	Statement stmt = null;
	String sql;
	ResultSet rs;
	public boolean connectDatabase(String bank_Name) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		String s=DB_URL+bank_Name;
		conn = DriverManager.getConnection(s,USER,PASS);
		stmt=conn.createStatement();
		return true;
	}
	
	public abstract void updateDetailsIntoDatabase(long act,int amt,char c) throws SQLException;
	
	public abstract void OfflineData() throws SQLException, IOException;
	
}