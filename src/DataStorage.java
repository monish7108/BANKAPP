import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class DataStorage extends Commit  {
	
	String name_Of_Customer;
	int account_Number,bal;
	String type_Of_Account,filename;
	int balance,choice,amt;
	long act;
	boolean flag;
	
	
	BufferedWriter out = null;
	
	
	public ArrayList<String> data = new ArrayList<String>();
	

	int query;
	
	
	
	Scanner scan=new Scanner(System.in);
	
		public void register() throws SQLException{
			
				System.out.println("enter name: ");
				
					name_Of_Customer=scan.nextLine();
				
		
				while(true){
					System.out.println("enter type of account: ");
					type_Of_Account=scan.nextLine();
					if(type_Of_Account.equalsIgnoreCase("savings") || type_Of_Account.equalsIgnoreCase("current"))
					break;
					else
					System.out.println("account type can only be savings or current");
				}
				while(true){
					System.out.println("enter initial balance: ");
					balance=scan.nextInt();
					if(balance < 500 )
					System.out.println("500 is initial balance...");
					else break;
				}
				scan.nextLine();
				
				act=(long)act_num_check();
				
				sql="insert into customerDetails () values(\""+name_Of_Customer+"\","+act+",\""+type_Of_Account+"\","+balance+");";
				
				query=stmt.executeUpdate(sql);
				
				
		}
	
		public long act_num_check() throws SQLException{
			sql="select account_Number from customerDetails order by account_Number desc limit 1";
			rs=stmt.executeQuery(sql);
			while(rs.next())
				account_Number=rs.getInt(1);
			
			return ++account_Number;
		}
		
		public void getDetails() throws SQLException{
			System.out.println("Enter account number:");
			act=scan.nextInt();
			
			sql="select * from customerDetails where account_Number="+act; //between "+from+" and "+upto;
			rs=stmt.executeQuery(sql);
			
			while (rs.next()) {
                String name = rs.getString("name_Of_Customer");
                int account = rs.getInt("account_Number");
                String type = rs.getString("type_Of_Account");
                int balanceOf = rs.getInt("balance");
                System.out.println("Name Of Customer: "+name + "\nA/C no: " + account + "\nType of A/c: " + type + "\nBalance: " + balanceOf+"\n");
                
			}
			
		}
		
		public void withdraw() throws SQLException{
			System.out.println("enter account number:");
			act=scan.nextInt();
			
			System.out.println("enter amount you want to withdraw");
			
			while(true){
			amt=scan.nextInt();
			if(amt%100==0)
				break;
			else
				System.out.println("enter amount in multiples of 100...");
			}
				System.out.println("Withdrawal Succesful....");
				updateDetailsIntoDatabase(act,amt,'W');
			
		}
		
		public void deposit() throws SQLException{
			System.out.println("enter account number:");
			act=scan.nextInt();
			
			System.out.println("enter amount you want to deposit");
			amt=scan.nextInt();
			System.out.println("Deposited Succesful....");
			updateDetailsIntoDatabase(act,amt,'D');
		}
		
		public void updateDetailsIntoDatabase(long act2, int amt, char c) throws SQLException {
			sql="select balance from customerDetails where account_Number="+act2;
			rs=stmt.executeQuery(sql);
			while(rs.next()){
				balance=rs.getInt(1);
			}
			System.out.println(balance);
			if(c=='D'){
				balance+=amt;
				
			}
			else{
				if(balance>amt){
					balance-=amt;
				}
				else{
					System.out.println("Insufficient balance....");
				}
			}
			
			sql="update customerDetails set balance="+balance+" where account_Number="+act2;
			query=stmt.executeUpdate(sql);
		}

		public void OfflineData() throws SQLException, IOException {
			int from,upto;
			System.out.println("from: ");
			from=scan.nextInt();
			System.out.println("upto: ");
			upto=scan.nextInt();
			sql="select * from customerDetails where account_Number between "+from+" and "+upto;
			rs=stmt.executeQuery(sql);
			
			
			while (rs.next()) {
                String name = rs.getString("name_Of_Customer");
                int account = rs.getInt("account_Number");
                String type = rs.getString("type_Of_Account");
                int balanceOf = rs.getInt("balance");
                data.add("Name Of Customer: "+name + "\nA/C no: " + account + "\nType of A/c: " + type + "\nBalance: " + balanceOf+"\n");
                
			}
			System.out.println("Enter file name: ");
            filename=scan.next();
            String path="/home/monish/WEEK-2[MONISH]/BANK[MONISH]-JDBC/"+filename;
			textFile(data,path);
            
		}
			
		public void textFile(ArrayList<String> daa, String path) throws IOException{
			
			File myfile = new File(path);
			myfile.createNewFile(); 
			try (BufferedWriter out = new BufferedWriter(
			        new FileWriter(path))) {
			    for(String s:data)
			    	out.write(s);
			} catch (IOException e) {
			    e.printStackTrace();
			}
		
		}
		
		
		public void closingDatabase(){
			try { rs.close(); } catch (Exception e) { System.out.println(e);}
		    try { stmt.close(); } catch (Exception e) { System.out.println(e); }
		    try { conn.close(); } catch (Exception e) { System.out.println(e); }
			
		}
}
